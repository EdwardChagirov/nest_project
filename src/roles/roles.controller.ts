import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { CreateRoleDto } from './dto/create-role.dto';
import { DeleteRoleDto } from './dto/delete-role.dto';
import { RolesService } from './roles.service';

@ApiTags('Roles')
@Controller('roles')
export class RolesController {
  constructor(private rolesService: RolesService) {}

  @Post()
  create(@Body() dto: CreateRoleDto) {
    return this.rolesService.create(dto);
  }

  @Get()
  getAll() {
    return this.rolesService.getAll();
  }

  @Get('/:value')
  getByValue(@Param('value') value: string) {
    return this.rolesService.getByValue(value);
  }

  @Delete()
  delete(@Body() dto: DeleteRoleDto) {
    return this.rolesService.delete(dto);
  }
}
