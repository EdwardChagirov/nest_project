import { ApiProperty } from '@nestjs/swagger';
import { Table, Column, Model, DataType, BelongsToMany } from 'sequelize-typescript';

import { User } from 'src/users/users.model';
import { UserRoles } from './user_roles.model';

interface RoleCreationAttr {
  value: string;
  description: string;
}

@Table({ tableName: 'roles' })
export class Role extends Model<Role, RoleCreationAttr> {
  @ApiProperty({
    example: '6e04286a-5976-4ba1-a400-8cb284a62b0c',
    description: 'Role ID',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    primaryKey: true,
    defaultValue: DataType.UUIDV4,
  })
  id: string;

  @ApiProperty({ example: 'ADMIN', description: 'Role value' })
  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  value: string;

  @ApiProperty({ example: 'Administrator', description: 'Role description' })
  @Column({ type: DataType.STRING })
  description: string;

  @BelongsToMany(() => User, () => UserRoles)
  users: User[];
}
