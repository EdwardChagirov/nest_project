import { Body, Controller, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';

import { CreatePostDto } from './dto/create-posts.dto';
import { PostsService } from './posts.service';

@ApiTags('Posts')
@Controller('posts')
export class PostsController {
  constructor(private postsService: PostsService) {}

  @Post()
  @UseInterceptors(FileInterceptor('image'))
  async create(@Body() dto: CreatePostDto, @UploadedFile() image: Express.Multer.File) {
    const post = await this.postsService.create(dto, image);
    return post;
  }
}
