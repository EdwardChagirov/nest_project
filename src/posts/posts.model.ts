import { ApiProperty } from '@nestjs/swagger';
import { Table, Column, Model, DataType, BelongsTo, ForeignKey } from 'sequelize-typescript';

import { User } from 'src/users/users.model';

interface PostCreationAttr {
  title: string;
  content: string;
  userId: string;
  image: string;
}

@Table({ tableName: 'posts' })
export class Post extends Model<Post, PostCreationAttr> {
  @ApiProperty({ example: '73638654-bd11-4a06-8b57-951f46570d0f', description: 'Post ID' })
  @Column({
    type: DataType.STRING,
    unique: true,
    primaryKey: true,
    defaultValue: DataType.UUIDV4,
  })
  id: string;

  @ApiProperty({ example: 'Lorem Ipsum', description: 'Post title' })
  @Column({ type: DataType.STRING, allowNull: false })
  title: string;

  @ApiProperty({ example: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit...', description: 'Post content' })
  @Column({ type: DataType.STRING })
  content: string;

  @ApiProperty({ example: '955f6220-5cd3-445a-98db-feb5c4f85880.jpg', description: 'Post image' })
  @Column({ type: DataType.STRING })
  image: string;

  @ForeignKey(() => User)
  @Column({ type: DataType.STRING })
  userId: string;

  @BelongsTo(() => User)
  author: User;
}
