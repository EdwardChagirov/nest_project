import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { FileService } from 'src/file/file.service';

import { CreatePostDto } from './dto/create-posts.dto';

import { Post } from './posts.model';

@Injectable()
export class PostsService {
  constructor(@InjectModel(Post) private postsRepository: typeof Post, private fileService: FileService) {}

  async create(dto: CreatePostDto, image: Express.Multer.File) {
    const fileName = await this.fileService.create(image);
    const post = await this.postsRepository.create({ ...dto, image: fileName });
    return post;
  }
}
