import { ApiProperty } from '@nestjs/swagger';

export class CreatePostDto {
  @ApiProperty({ example: 'Lorem Ipsum', description: 'Post title' })
  readonly title: string;

  @ApiProperty({ example: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit...', description: 'Post content' })
  readonly content: string;

  @ApiProperty({ example: '1e02a0c6-7ea7-4b50-81b8-05e1541af98a', description: 'User ID' })
  readonly userId: string;

  @ApiProperty({ example: '955f6220-5cd3-445a-98db-feb5c4f85880.jpg', description: 'Post image' })
  readonly image: string;
}
