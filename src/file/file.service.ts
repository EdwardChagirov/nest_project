import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

import * as fs from 'fs';
import * as uuid from 'uuid';
import * as path from 'path';

@Injectable()
export class FileService {
  async create(file: Express.Multer.File): Promise<string> {
    try {
      const fileName = uuid.v4() + '.jpg';
      const filePath = path.resolve(__dirname, '..', 'static');
      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath, { recursive: true });
      }
      fs.writeFileSync(path.join(filePath, fileName), file.buffer);
      return fileName;
    } catch (e) {
      throw new HttpException({ message: 'Произошла ошибка при записи файла' }, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
