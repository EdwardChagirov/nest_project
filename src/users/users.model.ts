import { ApiProperty } from '@nestjs/swagger';
import { Table, Column, Model, DataType, BelongsToMany, HasMany } from 'sequelize-typescript';

import { Post } from 'src/posts/posts.model';

import { Role } from 'src/roles/roles.model';
import { UserRoles } from 'src/roles/user_roles.model';

interface UserCreationAttr {
  email: string;
  password: string;
}

@Table({ tableName: 'users' })
export class User extends Model<User, UserCreationAttr> {
  @ApiProperty({
    example: '1e02a0c6-7ea7-4b50-81b8-05e1541af98a',
    description: 'User ID',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    primaryKey: true,
    allowNull: false,
    defaultValue: DataType.UUIDV4,
  })
  id: string;

  @ApiProperty({ example: 'chagirov@gmail.com', description: 'User ID' })
  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  email: string;

  @ApiProperty({ example: '123456', description: 'User password' })
  @Column({ type: DataType.STRING, allowNull: false })
  password: string;

  @ApiProperty({ example: false, description: 'User is banned' })
  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  banned: boolean;

  @ApiProperty({ example: 'Swearing', description: 'User ban reason' })
  @Column({ type: DataType.STRING, allowNull: true })
  banReason: string;

  @BelongsToMany(() => Role, () => UserRoles)
  roles: Role[];

  @HasMany(() => Post)
  posts: Post[];
}
