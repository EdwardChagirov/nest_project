import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsEmail, Length } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({ example: 'chagirov@gmail.com', description: 'User email' })
  @IsString({ message: 'Должна быть строка' })
  @IsEmail({}, { message: 'Неккоректный email' })
  readonly email: string;

  @ApiProperty({ example: '123456', description: 'User password' })
  @IsString({ message: 'Должна быть строка' })
  @Length(4, 10, { message: 'Диапазон 4 - 10 символов' })
  readonly password: string;
}
