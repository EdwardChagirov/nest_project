import { ApiProperty } from '@nestjs/swagger';

export class AddRoleDto {
  @ApiProperty({ example: '1e02a0c6-7ea7-4b50-81b8-05e1541af98a' })
  readonly userId: string;

  @ApiProperty({ example: 'ADMIN' })
  readonly value: string;
}
