import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { Role } from 'src/roles/roles.model';
import { ROLES_KEY } from './roles-auth.decorator';

@Injectable()
export class RolesAuthGuard implements CanActivate {
  constructor(private jwtService: JwtService, private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const requiredRoles = this.reflector.getAllAndOverride<string[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!requiredRoles) {
      return true;
    }

    const req = context.switchToHttp().getRequest();

    try {
      const authHeaders = req.headers.authorization;

      if (!authHeaders) {
        throw new UnauthorizedException({ message: 'Пользователь не авторизован', code: 'auth_error' });
      }

      try {
        const token = authHeaders.split(' ')[1];
        const user = this.jwtService.verify(token);
        req.user = user;
        return user.roles.some((role: Role) => requiredRoles.includes(role.value));
      } catch (e) {
        throw new UnauthorizedException({ message: 'Недействительный токен', code: 'token_error' });
      }
    } catch (e) {
      if (e) {
        const { response, status } = e || {};
        throw new HttpException({ message: response.message, code: response.code }, status);
      }
      throw new HttpException({ message: 'Нет доступа', code: 'forbidden' }, HttpStatus.FORBIDDEN);
    }
  }
}
